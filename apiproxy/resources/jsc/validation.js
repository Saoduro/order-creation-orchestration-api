var requestBody = JSON.parse(context.getVariable("request.content"));

context.setVariable("correlationId",requestBody.correlationId);

var timeStamp = new Date().toISOString().replace('Z', '');
context.setVariable("timeStamp", timeStamp);